package com.example.trimvideo.trim

import android.content.Context
import android.media.MediaExtractor
import android.media.MediaFormat
import android.util.Log
import com.example.trimvideo.trim.mp4parser.ClippedTrack
import com.example.trimvideo.trim.mp4parser.MyDefaultMp4Builder
import com.googlecode.mp4parser.authoring.Movie
import com.googlecode.mp4parser.authoring.Track
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator
import java.io.File
import java.io.FileOutputStream
import java.util.*
import java.util.concurrent.TimeUnit

class MP4parserTrimmer : Trimmer() {
    override fun getName(): String {
        return "mp4parser"
    }

    override fun trim(context: Context, inputPath: String, outputPath: String, soundEnabled: Boolean, startMs: Long, endMs: Long): Boolean {

        val movie: Movie = MovieCreator.build(inputPath);

        val tracks = movie.getTracks()
        if (tracks.size == 0) {
            return false
        }
        movie.setTracks(LinkedList<Track>());
        // remove all tracks we will create new tracks from the old

        var timeCorrected = false;

        // Here we try to find a track that has sync samples. Since we can only start decoding
        // at such a sample we SHOULD make sure that the start of the new fragment is exactly
        // such a frame
        var startTime = startMs.toDouble()


        val extractor = MediaExtractor()
        extractor.setDataSource(inputPath)
        val videoTrack = getVideoTrack(extractor)
        val duration = extractor.getTrackFormat(videoTrack).getLong(MediaFormat.KEY_DURATION)
        var endTime: Double = if (endMs > 0) {
            endMs.toDouble()
        } else {
            duration/1000.0
        }

        if (endTime - startTime > MAX_VIDEO_DURATION_SECONDS) {
            endTime = startTime + MAX_VIDEO_DURATION_SECONDS
        }

        for (track in tracks) {
            if (track.syncSamples != null && track.syncSamples.size == 0) {
                Log.e("Diana", "track.syncSamples is EMPTE")
            }
            if (track.syncSamples != null && track.syncSamples.size > 0) {
                if (timeCorrected) {
                    // This exception here could be a false positive in case we have multiple tracks
                    // with sync samples at exactly the same positions. E.g. a single movie containing
                    // multiple qualities of the same video (Microsoft Smooth Streaming file)
                    getCallback().onComplete(false)
                    throw RuntimeException("The startTime has already been corrected by another track with SyncSample. Not Supported.")
                }
                startTime = correctTimeToSyncSample(track, startTime, false)
                endTime = correctTimeToSyncSample(track, endTime, true)
                timeCorrected = true
            }
        }

        val runnable = Runnable {
            for (track in tracks) {
                var currentSample: Long = 0
                var currentTime = 0.0
                var lastTime = -1.0
                var startSample: Long = -1
                var endSample: Long = -1

                for (i in 0 until track.sampleDurations.size) {
                    val delta = track.sampleDurations[i]


                    if (currentTime > lastTime && currentTime <= startTime) {
                        // current sample is still before the new starttime
                        startSample = currentSample
                    }
                    if (currentTime > lastTime && currentTime <= endTime) {
                        // current sample is after the new start time and still before the new endtime
                        endSample = currentSample
                    }
                    lastTime = currentTime
                    currentTime += delta.toDouble() / track.trackMetaData.timescale.toDouble()
                    currentSample++
                }
                movie.addTrack(ClippedTrack(track, startSample, endSample))
            }
            val start1 = System.currentTimeMillis()
            val out = MyDefaultMp4Builder().build(movie)
            val start2 = System.currentTimeMillis()
            val fos = FileOutputStream(outputPath)
            val fc = fos.getChannel()
            out.writeContainer(fc)

            fc.close()
            fos.close()
            val start3 = System.currentTimeMillis()
           Log.e("Diana", "Building IsoFile took : " + (start2 - start1) + "ms")
           Log.e("Diana", "Writing IsoFile took  : " + (start3 - start2) + "ms")
           Log.e("Diana", 
                "Writing IsoFile speed : " + File(outputPath).length() / (start3 - start2) / 1000 + "MB/s"
            )
            getCallback().onComplete(true)
        }

        val thread = Thread(runnable)
        thread.start()

        return true
    }


    private fun correctTimeToSyncSample(track: Track, cutHere: Double, next: Boolean): Double {
        val timeOfSyncSamples = DoubleArray(track.syncSamples.size)
        var currentSample: Long = 0
        var currentTime = 0.0
        for (i in 0 until track.sampleDurations.size) {
            val delta = track.sampleDurations[i]

            if (Arrays.binarySearch(track.syncSamples, currentSample + 1) >= 0) {
                // samples always start with 1 but we start with zero therefore +1
                timeOfSyncSamples[Arrays.binarySearch(track.syncSamples, currentSample + 1)] = currentTime
            }
            currentTime += delta.toDouble() / track.trackMetaData.timescale.toDouble()
            currentSample++

        }
        var previous = 0.0
        for (timeOfSyncSample in timeOfSyncSamples) {
            if (timeOfSyncSample > cutHere) {
                return if (next) {
                    timeOfSyncSample
                } else {
                    previous
                }
            }
            previous = timeOfSyncSample
        }
        return timeOfSyncSamples[timeOfSyncSamples.size - 1]
    }

    private fun getVideoTrack(extractor: MediaExtractor): Int {
        for (i in 0 until extractor.trackCount) {
            if (extractor.getTrackFormat(i).getString(MediaFormat.KEY_MIME).startsWith("video/")) {
                return i
            }
        }
        return -1
    }

}
