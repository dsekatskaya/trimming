package com.example.trimvideo.trim

import android.content.Context
import com.example.trimvideo.trim.surface.TranscoderThread

class MuxerSurfaceTrimmer : Trimmer() {
    override fun getName(): String {
        return "muxer_surface"
    }

    override fun trim(context: Context, inputPath: String, outputPath: String, soundEnabled: Boolean, startMs: Long, endMs: Long): Boolean {

        val thread = TranscoderThread(inputPath, outputPath, startMs, endMs, soundEnabled) {
            getCallback().onComplete(true)
        }
        thread.start()
        return true
    }

}
