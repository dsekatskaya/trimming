package com.example.trimvideo.trim

import android.content.Context
import android.media.MediaMuxer
import java.io.IOException
import java.util.concurrent.TimeUnit

abstract class Trimmer {

    companion object {
        @JvmField public val MAX_VIDEO_DURATION_NS = TimeUnit.SECONDS.toMicros(30)
        val MAX_VIDEO_DURATION_SECONDS = TimeUnit.SECONDS.toMillis(30)
    }

    protected val mimeTypes: HashMap<String, Int> = hashMapOf(
        "video/mp4" to MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4,
        "video/webm" to MediaMuxer.OutputFormat.MUXER_OUTPUT_WEBM
    )

    private lateinit var callback: Callback



    /**
     * true means preparation is successful, not trimming process. Trim success dhould be relied on callback
     */
    @Throws(IOException::class)
    abstract fun trim(context: Context, inputPath: String, outputPath: String, soundEnabled: Boolean, startMs: Long, endMs: Long = 0): Boolean

    fun setCallback(callback: Callback) {
        this.callback = callback
    }

    fun getCallback() : Callback {

        if (!::callback.isInitialized) {
            callback = object : Trimmer.Callback {
                override fun onProgress(percent: Int) {
                    //
                }

                override fun onComplete(isSuccess: Boolean) {
                    //
                }

            }
        }

        return callback
    }


    abstract fun getName(): String

    interface Callback {
        fun onComplete(isSuccess: Boolean)

        /**
         * percent completed out 100
         */
        fun onProgress(percent: Int)
    }

    enum class Types {
        ffmpeg_codec, ffmpeg, MediaMuxer_surface, MediaMuxer_general, MP4parser
    }
}
