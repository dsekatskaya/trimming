package com.example.trimvideo.trim

import android.content.Context
import android.media.*
import android.util.Log
import java.io.FileInputStream
import java.nio.ByteBuffer

class MuxerTrimmer : Trimmer() {

    override fun getName(): String {
        return "muxer_gen"
    }

    companion object {
        const val TAG = "Diana"
        private val MAX_SAMPLE_SIZE = 256 * 1024
    }

    override fun trim(
        context: Context,
        inputPath: String,
        outputPath: String,
        soundEnabled: Boolean,
        startMs: Long,
        endMs: Long
    ): Boolean {
        val mediaMetadata = MediaMetadataRetriever()
        mediaMetadata.setDataSource(inputPath)

        val mimeType = mediaMetadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_MIMETYPE)

        Log.d(TAG, "Video mime type is ${mimeType}")

        val videoType = mimeTypes[mimeType] ?: return false

        val muxer = MediaMuxer(outputPath, videoType)

        val fd = FileInputStream(inputPath).fd

        val extractor = MediaExtractor()
        extractor.setDataSource(fd)
        val trackCount = extractor.trackCount
        val tracks = IntArray(trackCount)

        var videoTrack = -1
        var audioTrack = -1
        var bufferSize = -1

        if (trackCount < 0) {
            return false
        }
        var i = 0
        while (i < trackCount) {
            val trackFormat = extractor.getTrackFormat(i)
            val mime = trackFormat.getString(MediaFormat.KEY_MIME)

            var found = false
            if (mime.startsWith("video") && videoTrack < 0) {
                found = true
                videoTrack = i
            }

            if (mime.startsWith("audio") && audioTrack < 0 && soundEnabled) {
                found = true
                audioTrack = i
            }

            if (found) {
                tracks[i] = muxer.addTrack(trackFormat)

                if (trackFormat.containsKey(MediaFormat.KEY_MAX_INPUT_SIZE)) {
                    val maxBufferSize = trackFormat.getInteger(MediaFormat.KEY_MAX_INPUT_SIZE)

                    if (maxBufferSize > bufferSize) {
                        bufferSize = maxBufferSize
                    }
                }
            }
            i++
        }

        if (videoTrack >= 0) {
            extractor.selectTrack(videoTrack)
        }

        if (audioTrack >= 0) {
            extractor.selectTrack(audioTrack)
        }

        if (bufferSize < 0) {
            bufferSize = 256 * 1024
        }

        val rotationDegrees =
            mediaMetadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION).toInt()
        mediaMetadata.release()

        if (rotationDegrees != 0) {
            muxer.setOrientationHint(rotationDegrees)
        }

        val duration = extractor.getTrackFormat(videoTrack).getLong(MediaFormat.KEY_DURATION)
        Log.d(TAG, "Duration: $duration")

        val startNs = startMs * 1000
        val endNs: Long
        if (endMs > 0) {
            endNs = endMs * 1000
        } else {
            endNs = duration
        }

        // start is in ms but seekTo() takes ns
        extractor.seekTo(startNs, MediaExtractor.SEEK_TO_CLOSEST_SYNC)

        val offset = 0
        val byteBuffer = ByteBuffer.allocate(bufferSize)
        val bufferInfo = MediaCodec.BufferInfo()
        val runnable = Runnable {
            muxer.start()
            var isEnded = false
            var success = false

            var lastAudioPresentationTime = 0L
            while (!isEnded && bufferInfo.size >= 0) {
                bufferInfo.offset = offset
                bufferInfo.size = extractor.readSampleData(byteBuffer, offset)
                lastAudioPresentationTime = extractor.sampleTime
                bufferInfo.presentationTimeUs = lastAudioPresentationTime
//                        getProgressCallback().invoke(extractor.sampleTime / 1000)

                if (endNs > 0 && bufferInfo.presentationTimeUs >= endNs) {
                    Log.d(TAG, "Reached the end of the video")
                    success = true
                    isEnded = true
                    extractor.release()
                    break
                } else {
                    bufferInfo.flags = extractor.sampleFlags
                    val currentTrackIndex = extractor.sampleTrackIndex
                    if (currentTrackIndex >= 0) {
                        muxer.writeSampleData(tracks[currentTrackIndex], byteBuffer, bufferInfo)
                    }
                    extractor.advance()
                }
            }

            Log.d(TAG, "Done transcoding")

            try {
                muxer.stop()
                muxer.release()
            } catch (e: Exception) {
                Log.e(TAG, "Caught exception finalizing trimmed file", e)
                success = false
            } finally {
                getCallback().onComplete(success)
            }
        }


        val thread = Thread(runnable)
        thread.start()
        return true
    }

}
