package com.example.trimvideo.trim

import android.content.Context
import android.media.MediaExtractor
import android.media.MediaFormat
import android.media.MediaMetadataRetriever
import android.os.Build
import android.util.Log
import com.github.hiteshsondhi88.libffmpeg.FFmpeg
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler
import com.github.hiteshsondhi88.libffmpeg.FFmpegLoadBinaryResponseHandler
import java.util.concurrent.TimeUnit


class FFMpegTrimmer(val isCodecEnabled: Boolean = false) : Trimmer() {



    override fun getName(): String {
        return "ffmpeg"
    }

    override fun trim(
        context: Context,
        inputPath: String,
        outputPath: String,
        soundEnabled: Boolean,
        startTrimMs: Long,
        endTrimMs: Long
    ): Boolean {
        val extractor = MediaExtractor()
        extractor.setDataSource(inputPath)
        val trackCount = extractor.trackCount

        var videoTrack = -1

        if (trackCount > 0) {
            var i = 0
            while (i < trackCount) {
                val trackFormat = extractor.getTrackFormat(i)
                val mime = trackFormat.getString(MediaFormat.KEY_MIME)
                if (mime.startsWith("video") && videoTrack < 0) {
                    videoTrack = i
                    break
                }
                i++
            }
        }
        if (videoTrack == -1) {
            return false
        }

        val startMs = startTrimMs
        var endMs = if (endTrimMs > 0) {
            endTrimMs
        } else {
            extractor.getTrackFormat(videoTrack).getLong(MediaFormat.KEY_DURATION)
        }

        if (endMs - startMs > MAX_VIDEO_DURATION_SECONDS) {
            endMs = startMs + MAX_VIDEO_DURATION_SECONDS
        }

        Log.d("Diana", "Trim NEW $inputPath to $outputPath start $startMs end $endMs")
        //no op


        logDetails(inputPath)

        val duration: Float = (endMs - startMs).toFloat() / 1000

        val startString: Float = startMs.toFloat() / 1000
        val endString: Float = endMs.toFloat() / 1000
        Log.e("Diana", "trim duration " + duration + " startString " + startString + " endString " + endString);
        val complexCommand: Array<String> =
            if (soundEnabled) {
                val mediaMetadata = MediaMetadataRetriever()
                mediaMetadata.setDataSource(inputPath)
                val bitrate = mediaMetadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_BITRATE);

                if (isCodecEnabled) {
                    arrayOf(
                        /*trim*/ "-ss", "" + startString, /*"-t", "" + endString, */"-y", "-i", inputPath, "-t", "" + duration,
                        //"-s", heightAndWidth, "-r", (if (frameRate == null) "30" else frameRate), -> makes it slower to process
                        /*use codec*/ "-vcodec", "mpeg4", /*"-b:v", bitrate.toString(),*/
                        //for audio
                        //"-b:a", "48000", "-ac", "2", "-ar", "22050",
                        "-c:v","copy", "-c:a", "copy", outputPath
                    )
                } else {
                    arrayOf(
                        "-ss", "" + startString, "-y", "-i", inputPath,
                        "-t", "" + duration, outputPath
                    )
                }
            } else {
                if (isCodecEnabled) {
                    val mediaMetadata = MediaMetadataRetriever()
                    mediaMetadata.setDataSource(inputPath)
                    val bitrate = mediaMetadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_BITRATE);
                    arrayOf(
                        /*trim*/ "-ss", "" + startString, "-y", "-i", inputPath, "-t", "" + duration,
                        /*use codec*/ "-vcodec", "mpeg4", "-b:v", bitrate.toString(),
                        "-c:v","copy", "-c:a", "copy", "-an", outputPath
                    )
                } else {
                    arrayOf(
                        "-ss", "" + startString, "-y", "-i", inputPath,
                        "-t", "" + duration, "-an", outputPath
                    )
                }
            }


        val ffmpeg = FFmpeg.getInstance(context)

        ffmpeg.loadBinary(object : FFmpegLoadBinaryResponseHandler {
            override fun onFinish() {
            }

            override fun onStart() {
            }

            override fun onSuccess() {
                ffmpeg.execute(
                    complexCommand,
                    object : FFmpegExecuteResponseHandler {
                        override fun onFinish() {

                        }

                        override fun onSuccess(message: String?) {
                            Log.e("Diana", "onSuccess ")
                            logDetails(outputPath)
                            getCallback().onComplete(true)
                        }

                        override fun onFailure(message: String?) {
                            Log.e("Diana", "onFailure " + message)
                            getCallback().onComplete(false)
                        }

                        override fun onProgress(message: String?) {
//                            Log.e("Diana", "message " + message)
//                        progressCallback.onProgress(extractor.sampleTime / 1000)
                        }

                        override fun onStart() {
                            Log.e("Diana", "onStart ")
                        }
                    }
                )
            }

            override fun onFailure() {
                getCallback().onComplete(false)
            }

        })
        return true
    }

    private fun logDetails(path: String) {
        val mediaMetadata = MediaMetadataRetriever()
        mediaMetadata.setDataSource(path)
        val bitrate = mediaMetadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_BITRATE);
        val heightAndWidth = mediaMetadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH) + "x" +
                mediaMetadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT)
        val frameRate = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mediaMetadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_CAPTURE_FRAMERATE)
        } else {
            "not supported"
        }
        val frameCount = mediaMetadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_FRAME_COUNT)
        Log.e("Diana", "$path bitRate `$bitrate`  `$heightAndWidth` frameCount `$frameCount`  frameRate `$frameRate`")
    }

}