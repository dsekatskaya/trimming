package com.example.trimvideo

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaExtractor
import android.media.MediaFormat
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.trimvideo.PlayVideoActivity.Companion.EXTRA_PATH
import com.example.trimvideo.trim.*
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File


class MainActivity : AppCompatActivity(), Trimmer.Callback {

    var endMaxNs: Long = 0L
    var startTrimmingMS = 0L
    val trimmers: Array<String> = Array(Trimmer.Types.values().size) { i -> Trimmer.Types.values()[i].name }
    var tempFilePath: String? = null

    companion object {
        private const val PICKFILE_REQUEST_CODE: Int = 785
        private const val GRANT_PERMISSION_REQUEST_CODE: Int = 798
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, trimmers)

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
        spinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                clearView()
                buttonStartTrim.isEnabled = true
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                //no op
            }


        }

        img_tv_action.setOnClickListener { v ->
            val intent = Intent(Intent.ACTION_GET_CONTENT, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
            intent.type = "video/*"
            startActivityForResult(intent, PICKFILE_REQUEST_CODE)
        }

        buttonStartTrim.setOnClickListener { v ->
            val path = textView.text.toString()
            if (!TextUtils.isEmpty(path)) {
                buttonStartTrim.isEnabled = false
                clearView()

                val startMs: Long = Integer.parseInt(startSeconds.text.toString()) * 1000L
                val endMs: Long = if (endSeconds.text.isEmpty() || TextUtils.isEmpty(endSeconds.text.toString())) {
                    -1
                } else {
                    Integer.parseInt(endSeconds.text.toString()) * 1000L
                }

                val trimmer = getTrimmer()

                val localFile = File(path)
                val file: File = FileUtils.getTempFile(externalCacheDir, trimmer.getName(), localFile.name, startMs, endMs)
                tempFilePath = file.absolutePath

                trimmer.setCallback(this)

                startTrimmingMS = System.currentTimeMillis()
                val result = trimmer.trim(this, localFile.absolutePath, tempFilePath!!, soundEnabled.isChecked, startMs, endMs)
                if (!result) {
                    textViewResult.text = "Fail Preparation"
                } else {
                    textViewResult.text = "Trimming in Progress"
                }
            }
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                GRANT_PERMISSION_REQUEST_CODE
            )
        }

        playVideo.setOnClickListener {
            if (tempFilePath != null) {
                val intent = Intent(this, PlayVideoActivity::class.java).apply {
                    putExtra(EXTRA_PATH, tempFilePath)
                }
                startActivity(intent)
            }
        }
    }

    private fun clearView() {
        textViewResult.text = ""
        playVideo.visibility = View.INVISIBLE
    }

    private fun getTrimmer(): Trimmer {
        val selectedTrimmer = trimmers.get(spinner.selectedItemPosition)
        Log.e("Diana", selectedTrimmer);

        return when (selectedTrimmer) {
            Trimmer.Types.MediaMuxer_general.name -> MuxerTrimmer()
            Trimmer.Types.MP4parser.name -> MP4parserTrimmer()
            Trimmer.Types.MediaMuxer_surface.name -> MuxerSurfaceTrimmer()
            Trimmer.Types.ffmpeg.name -> FFMpegTrimmer()
            Trimmer.Types.ffmpeg_codec.name -> FFMpegTrimmer(true)
            else -> MuxerTrimmer()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == PICKFILE_REQUEST_CODE && data != null) {
            val path = PathUtils.getPath(this, data.data)
            if (path != null) {
                buttonStartTrim.isEnabled = true
                textView.text = path
                endSeconds.text.clear()
                clearView()

                val extractor = MediaExtractor()
                extractor.setDataSource(path)
                var videoTrack = -1

                if (extractor.trackCount > 0) {
                    var i = 0
                    while (i < extractor.trackCount) {
                        val trackFormat = extractor.getTrackFormat(i)
                        val mime = trackFormat.getString(MediaFormat.KEY_MIME)
                        if (mime.startsWith("video") && videoTrack < 0) {
                            videoTrack = i
                            break
                        }
                        i++
                    }
                }
                if (videoTrack == -1) {
                    return
                }

                val duration = extractor.getTrackFormat(videoTrack).getLong(MediaFormat.KEY_DURATION)
                Log.d("Diana", "Duration: $duration")

                endMaxNs = duration / 1000
                startSeconds.setText("0")
                endSeconds.setHint((endMaxNs / 1000).toString())
                extractor.release()
            }
        }
    }

    override fun onComplete(isSuccess: Boolean) {
        Log.e("Diana", "startTrimmingMS " + startTrimmingMS + " " + System.currentTimeMillis())
        runOnUiThread {
            val duration = System.currentTimeMillis() - startTrimmingMS
            if (isSuccess) {
                playVideo.visibility = View.VISIBLE
                textViewResult.text ="trimmed in $duration \n$tempFilePath"
            } else {
                playVideo.visibility = View.INVISIBLE
                textViewResult.text = "failed in $duration \n$tempFilePath"
            }
            buttonStartTrim.isEnabled = true
        }
    }

    override fun onProgress(percent: Int) {
        //no op
    }

}
