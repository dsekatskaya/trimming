package com.example.trimvideo

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.MediaStore


open class PathUtils {

    companion object {
        fun getPath(context: Context, uri: Uri?): String? {
            if (uri == null) {
                return null
            }
            if ("content".equals(uri.getScheme(), ignoreCase = true)) {
                return if (isGooglePhotosUri(uri)) uri.getLastPathSegment() else getDataColumn(context, uri)
            } else if ("file".equals(uri.getScheme(), ignoreCase = true)) {
                return uri.getPath()
            }

            return null
        }

        private fun getDataColumn(context: Context, uri: Uri): String? {
            var cursor = context.contentResolver.query(uri, null, null, null, null)
            if (cursor == null || !cursor.moveToFirst()) {
                return null
            }
            var document_id = cursor.getString(0)
            document_id = document_id.substring(document_id.lastIndexOf(":") + 1)
            cursor.close()

            cursor = context.contentResolver.query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                null,
                MediaStore.Video.Media._ID + " = ? ",
                arrayOf<String>(document_id),
                null
            )
            if (cursor == null || !cursor.moveToFirst()) {
                return null
            }

            val path = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA))
            cursor.close()

            return path


        }

        fun isGooglePhotosUri(uri: Uri): Boolean {
            return "com.google.android.apps.photos.content" == uri.getAuthority()
        }
    }
}