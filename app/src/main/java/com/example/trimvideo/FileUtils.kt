package com.example.trimvideo

import java.io.File

open class FileUtils {

    companion object {
        fun getTempFile(cacheDir: File, trimmer: String, fileName: String, start: Long, end: Long = -1): File {
            val i = fileName.lastIndexOf('.')
            val name: String
            val extension: String
            if (i > 0) {
                name = fileName.substring(0, i)
                extension = fileName.substring(i)
            } else {
                name = fileName
                extension = ""
            }

            val file = File(cacheDir, "$trimmer-$name-$start-$end$extension")
            if (file.exists()) {
                file.delete()
            }
            return file
        }
    }

}
