package com.example.trimvideo

import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.activity_play_video.*

class PlayVideoActivity : AppCompatActivity() {

    private var player: SimpleExoPlayer? = null
    private lateinit var path: String

    companion object {
        const val EXTRA_PATH = "EXTRA_PATH"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_play_video)
        path = intent.getStringExtra(EXTRA_PATH)
        Log.e("Diana", "EXTRA_PATH " + path);
    }

    override fun onResume() {
        super.onResume()
        initializePlayer()
    }

    override fun onPause() {
        super.onPause()
        releasePlayer()
    }

    private fun initializePlayer() {
        if (player == null) {
            player = ExoPlayerFactory.newSimpleInstance(
                DefaultRenderersFactory(this),
                DefaultTrackSelector(),
                DefaultLoadControl()
            )
            playerView.setPlayer(player)
            player?.setPlayWhenReady(true)
            player?.seekTo(0, 0)
        }
        val mediaSource = buildMediaSource()
        player?.prepare(mediaSource, true, false)
    }

    private fun buildMediaSource(): MediaSource {

        val userAgent = "exoplayer-codelab"
        val uri = Uri.parse(path)

//        if (uri.lastPathSegment.contains("mp4")) {
//            return ExtractorMediaSource.Factory(DefaultHttpDataSourceFactory(userAgent))
//                .createMediaSource(uri)
//        } else {
//            val dashChunkSourceFactory = DefaultDashChunkSource.Factory(
//                DefaultHttpDataSourceFactory("ua", BANDWIDTH_METER)
//            )
//            val manifestDataSourceFactory = DefaultHttpDataSourceFactory(userAgent)
//            return DashMediaSource.Factory(dashChunkSourceFactory, manifestDataSourceFactory).createMediaSource(uri)
//        }

        val bandwidthMeter = DefaultBandwidthMeter()
        val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)
        val trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)

        val defaultBandwidthMeter = DefaultBandwidthMeter()
        val dataSourceFactory = DefaultDataSourceFactory(
            this, Util.getUserAgent(this, "TextTrim"), defaultBandwidthMeter)
        return ExtractorMediaSource.Factory(dataSourceFactory).
            createMediaSource(uri)
    }

    private fun releasePlayer() {
        player?.release()
        player = null
    }
}
